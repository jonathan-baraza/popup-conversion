$(document).ready(function () {
  $("#divTwoHolder").hide();
  $("#openForm").click(function () {
    $("#openForm").fadeOut();
    $("#form").fadeIn();
  });
  $("#closeForm").click(function () {
    $("#openForm").fadeIn();
    $("#form").fadeOut();
  });

  $("#selector").click(function () {
    const isClosed = $("#selectorToggle").hasClass("fa-chevron-down");
    console.log(isClosed);
    if (isClosed) {
      $("#selectorToggle")
        .removeClass("fa-chevron-down")
        .addClass("fa-chevron-up");
      $("#divTwoHolder").show();
    } else {
      $("#selectorToggle")
        .removeClass("fa-chevron-up")
        .addClass("fa-chevron-down");
      $("#divTwoHolder").hide();
    }
  });
});
